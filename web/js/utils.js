/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Formata um campo inputText de acordo com a máscara passado 
 * @param {type} mascara
 * @param {type} documento
 * @returns {undefined}
 */
function formatar(mascara, documento){
    var i = documento.value.length;
    var saida = mascara.substring(0,1);
    var texto = mascara.substring(i);
    
    if (texto.substring(0,1) != saida){
        documento.value += texto.substring(0,1);
    }
}

/**
 * Formata um campo de texto de acordo com uma máscara de telefone de 8 ou 9 dígitos
 * @param {type} campo
 * @returns {undefined}
 */
function mascaraTelefone( campo ) {
			
    function trata( valor,  isOnBlur ) {
					
        valor = valor.replace(/\D/g,"");             			
        valor = valor.replace(/^(\d{2})(\d)/g,"($1)$2"); 		

        if( isOnBlur ) {			
            valor = valor.replace(/(\d)(\d{4})$/,"$1-$2");   
        } else {
            valor = valor.replace(/(\d)(\d{3})$/,"$1-$2"); 
        }
        return valor;
    }

    campo.onkeypress = function (evt) {

        var code = (window.event)? window.event.keyCode : evt.which;	
        var valor = this.value

        if(code > 57 || (code < 48 && code != 8 ))  {
            return false;
        } else {
            this.value = trata(valor, false);
        }
    }

    campo.onblur = function() {

        var valor = this.value;

        if( valor.length < 13 ) {
            this.value = ""
        }else {		
            this.value = trata( this.value, true );
        }
    }

    campo.maxLength = 14;
}

/**
 * Exibe ou oculta um componente passado por parâmetro
 * @param {type} show
 * @returns {undefined}
 */
function exibe(show,elemento) {
    // Get the panel using its ID
    var obj = document.getElementById(elemento);

    if (show) {
        obj.style.display = "block";
    } else {
        obj.style.display = "none";
    }
}

/**
 *  Função que retorna o dia de hoje
 * @param {type} campo
 * @returns {undefined}
 */
function getDate(campo){
    // Obtém a data
    var today = new Date();
    // Obtém o dia
    var dd = today.getDate();
    // Obtèm o mês
    var mm = today.getMonth()+1;
    // Obtém o ano
    var yyyy = today.getFullYear();

    // Se o dia é menor que 10, acrescenta um '0'
    if(dd<10) {
        dd='0'+dd
    } 

    // Se o mês é menos que 10, acrescenta um '0'
    if(mm<10) {
        mm='0'+mm
    } 

    campo = mm+'/'+dd+'/'+yyyy;
}