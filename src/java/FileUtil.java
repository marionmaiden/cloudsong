
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe que gerencia o sistema de arquivos
 *
 * @author mario
 */
public class FileUtil implements Serializable {

    // Array com os nomes de arquivos encontrados em uma determinada pasta
    private List<String> files;
    // Array com os nomes de pastas encontradas dentro de outra pasta
    private List<String> folders;

    /**
     * Dado um endereço do sistema de arquivos, obtém uma lista de arquivos e de
     * pastas
     *
     * @param path caminho onde será feita a pesquisa
     */
    public void listFiles(String path) {

        // Inicializa os arrays de arquivos e de pastas
        setFiles(new ArrayList<String>());
        setFolders(new ArrayList<String>());

        // Objeto file que conterá a pasta a ser navegada
        File fol;

        try {
            fol = new File(path).getCanonicalFile();

            if (fol.isDirectory()) {
                File[] v = fol.listFiles();

                for (File v1 : v) {
                    // Caso seja uma pasta
                    if (v1.isDirectory()) {
                        getFolders().add(v1.getPath());
                    } else {
                        // Se for um áudio, adiciona ela à lista de retornos
                        if (v1.getName().endsWith(".mp3")) {
                            getFiles().add(v1.getPath().substring(PropertyReader.getProp("rootFolder").length() + 1));
                        }
                    }
                }

                Collections.sort(files);

            } else {
                System.err.println("Pasta inexistente");
            }

        } catch (IOException ex) {
            System.err.println("CAMINHO NÃO ENCONTRADO");
        }
    }

    /**
     * @return the files
     */
    public List<String> getFiles() {
        return files;
    }

    /**
     * @param files the files to set
     */
    public void setFiles(List<String> files) {
        this.files = files;
    }

    /**
     * @return the folders
     */
    public List<String> getFolders() {
        return folders;
    }

    /**
     * @param folders the folders to set
     */
    public void setFolders(List<String> folders) {
        this.folders = folders;
    }

}
