import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author mario
 */
@SessionScoped
@ManagedBean
public class MusicaBean implements Serializable {

    private FileUtil fu;
    private List musicas;
    private List pastas;
    private String musicaPlay;
    private String selectedPasta;

    /**
     * @return the fu
     */
    public FileUtil getFu() {
        return fu;
    }

    /**
     * @param fu the fu to set
     */
    public void setFu(FileUtil fu) {
        this.fu = fu;
    }

    @PostConstruct
    public void init() {

        selectedPasta = (selectedPasta == null || selectedPasta.length() < PropertyReader.getProp("rootFolder").length()) 
                ? PropertyReader.getProp("rootFolder") 
                : selectedPasta;

        fu = new FileUtil();
        fu.listFiles(selectedPasta);

        pastas = new ArrayList();
        // Não adiciona a pasta de nível superior caso seja a pasta raiz
        if(!selectedPasta.equals(PropertyReader.getProp("rootFolder"))){
            // Diferentiate windows and Linux slash use
            if(selectedPasta.lastIndexOf("/") > 0){
                pastas.add(selectedPasta.substring(0, selectedPasta.lastIndexOf("/")));
            }
            else{
                pastas.add(selectedPasta.substring(0, selectedPasta.lastIndexOf("\\")));
            }
        }
        pastas.addAll(fu.getFolders());

        if (fu.getFiles().size() > 0) {
            setMusicaPlay(fu.getFiles().get(0));
            musicas = new ArrayList(fu.getFiles());
        } else {
            musicas = new ArrayList();
            setMusicaPlay("");
        }

    }

    public String trocaPasta() {
        init();

        return "index";
    }

    public String troca() {

        if (musicas != null && !musicas.isEmpty()) {
            int index = musicas.indexOf(musicaPlay);
            musicaPlay = (String) musicas.get((index + 1) % musicas.size());
        }
        return "index";
    }

    /**
     * @return the pastas
     */
    public List getPastas() {
        return pastas;
    }

    /**
     * @param pastas the pastas to set
     */
    public void setPastas(List pastas) {
        this.pastas = pastas;
    }

    /**
     * @return the selectedPasta
     */
    public String getSelectedPasta() {
        return selectedPasta;
    }

    /**
     * @param selectedPasta the selectedPasta to set
     */
    public void setSelectedPasta(String selectedPasta) {
        this.selectedPasta = selectedPasta;
    }

    /**
     * @return the musicas
     */
    public List getMusicas() {
        return musicas;
    }

    /**
     * @param musicas the musicas to set
     */
    public void setMusicas(List musicas) {
        this.musicas = musicas;
    }

    /**
     * @return the musicaPlay
     */
    public String getMusicaPlay() {
        return musicaPlay;
    }

    /**
     * @param musicaPlay the musicaPlay to set
     */
    public void setMusicaPlay(String musicaPlay) {
        this.musicaPlay = musicaPlay;
    }

    public String getMusicaPlayName() {
        if (musicaPlay.length() > 0) {
            return musicaPlay.substring(musicaPlay.lastIndexOf("/") + 1);
        } else {
            return "";
        }
    }

    public String getSelectedPastaName() {
        if (selectedPasta.length() > 0) {
            return selectedPasta.substring(selectedPasta.lastIndexOf("/") + 1);
        } else {
            return "";
        }
    }

    public String getMusicaPlayName(String musica) {
        if (musica.length() > 0) {
            return musica.substring(musica.lastIndexOf("/") + 1);
        } else {
            return "";
        }
    }

}
