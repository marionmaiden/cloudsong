import java.util.ResourceBundle;

/**
 *
 * @author mario
 */
public class PropertyReader {

    public static String getProp(String propertyName) {

        ResourceBundle rb = ResourceBundle.getBundle("cloudSong");
        return rb.getString(propertyName);
    }

}
